@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
         @include('site/partials/sidebar-contact')     
               
          <div class="col-sm-8 blog-main">   
            <div class="blog-post">				
				
				@if (isset($items))   			    
				    <h1 class="blog-post-title">Our Team</h1>
				         
					@foreach ($items as $item)
						<div class="team-div">	
							<div class="team-details">
								<h2>{{$item->name}}</h2>

								@if ($item->role != "")
									<h3>{{$item->role}}</h3>
								@endif

								@if ($item->mobile != "")
									<div class="team-field">
										<div class="team-field-txt">Mobile</div>
										<div class="team-field-val"><a href="tel:{{ str_replace(' ', '', $item->mobile) }}">{{$item->mobile}}</a></div>
									</div>
								@endif

								@if ($item->phone != "")
									<div class="team-field">
										<div class="team-field-txt">Phone</div>
										<div class="team-field-val"><a href="tel:{{ str_replace(' ', '', $item->phone) }}">{{$item->phone}}</a></div>
									</div>
								@endif

								@if ($item->fax != "")
									<div class="team-field">
										<div class="team-field-txt">Fax</div>
										<div class="team-field-val">{{$item->fax}}</div>
									</div>
								@endif

								@if ($item->email != "")
									<div class="team-field">
										<div class="team-field-txt">Email</div>
										<div class="team-field-val"><a href="mailto:{{ $item->email }}">{{$item->email}}</a></div>
									</div>
								@endif
							</div>

							<div class="team-photo">
							   @if ($item->photo != "")
								   <img src="{{ url('') }}/{{$item->photo}}" alt="{{$item->name}}">
							   @endif
							</div>
						</div>

						<div class="team-description">{!! $item->description !!}</div>      				    

						<hr>                             					                             					
					@endforeach 
				@endif			     
         
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

       </div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.blog-masthead -->

@endsection
