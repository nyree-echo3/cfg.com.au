<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner carousel-inside">           
			<div class="carousel-item active">
		  	
		      @php
	            $imgUrl = "files/images/ss10.jpg";
	            $imgAlt = "";
	            
	            // Page Module Category
		        if (isset($category) && sizeof($category) > 0)  {
		           $imgUrl = $category[0]->header;
		           $imgAlt = $category[0]->name;
		        }	
		       
		        // Module
		        if (isset($module))  {
		           $imgUrl = $module->header_image;
		           $imgAlt = $module->display_name;
		        }	 	        	        
		      @endphp
		      
			  <img class="slide" src="{{ url('') }}/{{ $imgUrl }}" alt="{{ $imgAlt }}">
			  <div class="container">
				<div class="carousel-caption text-left">						  
				</div>
			  </div>
			</div>
       
  </div>
</div>


<div id="quick-buttons">
	<a href="{{ url('') }}/newsletter"><div class="quick-buttons-icon"><span class='line-1'>Sign me up</span><br><span class='line-2'>for the latest news</span></div></a>
</div>