<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!--
  <ol class="carousel-indicators">
    <?php $counter = 0; ?>
    @foreach($images as $image)        
       <li data-target="#myCarousel" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>       
       <?php $counter++;  ?>
    @endforeach 
  </ol>
  -->
  <div class="carousel-inner">
    
        <?php $counter = 0; ?>
        @foreach($images as $image) 
            <?php $counter++;  ?>
               
			<div class="carousel-item {{ $counter == 1 ? ' active' : '' }} carousel-item-overlay">
		      <div class="carousel-item-img">
			  <img class="slide" src="{{ url('') }}/{{ $image->location }}" alt="{{ $image->title }}">
			  </div>
			  <div class="container">
				<div class="carousel-caption text-left">
				  @if ( $image->title != "") <h1>{{ $image->title }}</h1> @endif
				  @if ( $image->description != "") <p>{{ $image->description }}</p> @endif
				  @if ( $image->url != "")<p><a class="btn btn-lg btn-primary" href="{{ $image->url }}" role="button">Click for more</a></p> @endif
				</div>
			  </div>
			</div>
        @endforeach
   
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<div id="quick-buttons">
	<a href="{{ url('') }}/newsletter"><div class="quick-buttons-icon"><span class='line-1'>Sign me up</span><br><span class='line-2'>for the latest news</span></div></a>
</div>

@include('site/partials/index-services')
