<!--
https://support.connective.com.au/hc/en-us/articles/115010005727-How-to-change-Calculators
-->
<script src="https://mercury.connective.com.au/calculators/js/CodeEmbeder.js" type="text/javascript"></script>

<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="borrowing-capacity-tab" data-toggle="tab" href="#borrowing-capacity" role="tab" aria-controls="home" aria-selected="true">Borrowing Capacity</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="repayments-tab" data-toggle="tab" href="#repayments" role="tab" aria-controls="profile" aria-selected="false">Extra Repayments</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="property-fees-tab" data-toggle="tab" href="#property-fees" role="tab" aria-controls="contact" aria-selected="false">Property Fees</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="borrowing-capacity" role="tabpanel" aria-labelledby="home-tab">
        <div id="connective-borrowing-capacity-calculator"></div>
    </div>
    <div class="tab-pane fade" id="repayments" role="tabpanel" aria-labelledby="profile-tab">
        <div id="connective-repayments-calculator"></div>
    </div>
    <div class="tab-pane fade" id="property-fees" role="tabpanel" aria-labelledby="contact-tab">
        <div id="connective-property-fees-calculator"></div>
    </div>
</div>
