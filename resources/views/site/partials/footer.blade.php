@include('site/partials/footer-links')

<footer class='footer'>
 <div class='footerContactsWrapper'>
	 <div class='footerContacts'>
		<div class="row">
		   <div class="col-lg-3 col-logo">
		      <div class="footer-logo">
			     <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-reverse.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>	
			  </div>
				  
			  <div id="footer-social">
			    <span>Follow us on </span>
				@if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif            			
				@if ( $social_linkedin != "") <a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif 				
				@if ( $social_instagram != "") <a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a> @endif
			  </div>
		   </div>

		   <div class="col-lg-3">
			  @if(count($modules))			   
			     @foreach($modules as $nav_item)   
			        @if ($nav_item["slug"] == "services")		           
			            @if(isset($nav_item["nav_sub"])) 		               
			               @foreach($nav_item["nav_sub"] as $nav_sub)				                 
					             <div class="footer-txt footer-menu"><a href="{{ url('') }}/pages/{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}">{{ $nav_sub["title"] }}</a></div>
                             
                           @endforeach
	                    @endif
		            @endif
			     @endforeach
		      @endif
		   </div>  
		   
		   <div class="col-lg-3">
		      <div class="footer-txt footer-menu"> <a href="{{ url('') }}">Home</a></div>
			  @if(count($modules))			   
			     @foreach($modules as $nav_item)   
			        @if ($nav_item["slug"] != "other" && $nav_item["slug"] != "calculators" && $nav_item["slug"] != "apply-now") 
					    <div class="footer-txt footer-menu"><a href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}">{{ $nav_item["display_name"] }}</a></div>
		            @endif
			     @endforeach
		      @endif
		   </div>  

		   <div class="col-lg-3">
			  <div class="footer-contact">
				  <h2>Call or email us</h2>
				  <div><a href="tel:{{ str_replace(' ', '', $phone_number) }}">T  {{ $phone_number }}</a></div>
				  <div><a href="mailto:{{ $contact_email }}">E  {{ $contact_email }}</a></div>
			  </div>
		   </div>  
		</div>
	 </div> 
 </div>
 
  <div class='footerContainer'>
  <div class='footerDisclaimer'>  
	  <div class="footer-disclaimer"> 
		<strong>Disclaimer</strong><br>
		This website provides general information only and has been prepared without taking into account your objectives, financial situation or needs. We recommend that you consider whether it is appropriate for your circumstances. Your full financial situation will need to be reviewed prior to acceptance of any offer or product. Subject to lenders terms and conditions. Fees and charges and eligibility criteria apply. We recommend that you seek your own independent financial, legal and taxation advice before making an investment decision. We do not provide financial or investment advice.
	  </div>
  </div>
</div>

 <div class='footerContainer'>  
  <div id="footer-txt"> 	                            
    @if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }}</a> | @endif
    <a href="{{ url('') }}/pages/other/privacy-statement">Privacy Statement</a> |     
    <a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a>    
    
    <br><br><br>
       
    <strong>Dandee Pty Ltd T/A Credit Finance Group (ABN 58 314 918 144)</strong><br>
    Crest Financial Group Pty Ltd T/A Crest Financial Group (ABN 34 104 629 227)<br>
    <i>Credit Representatives 425933 & 377270 are authorised under Australian Credit Licence 389328.</i><br>
  </div>
</div>

</footer>