<!--
https://support.connective.com.au/hc/en-us/articles/360008234993-Contact-us-and-Subscribe-to-newsletter
-->

<style>
.the-carrot {
  display: none;
}
</style>
<!--  --> 
<script type="text/javascript">
function validateForm(){
var namef=document.forms["contact"]["namef"].value;
var namel=document.forms["contact"]["namel"].value;
var email=document.forms["contact"]["email"].value;
if (namef==null || namef==""){  alert("Please enter your first name"); return false; }
if (namel==null || namel==""){  alert("Please enter your last Name"); return false;}
if (email==null || email==""){  alert("Please enter your email"); return false;}
}
</script>
<!--  --> 
<div class="shell"> 
    <form id="contact" action="https://m5.connective.com.au/api/Leads/LeadHandler.jsp" method="POST" onsubmit="return validateForm()">
    <input  class="the-carrot" id="name" type="text" name="name" size="25" value="" autocomplete="off"/>
    <div class="rendered-form">
		<div class="fb-text form-group">
		  <label class="fb-text-label">First Name<span class="fb-required">*</span></label>
		  <input name="namef" type="text" maxlength="40"  id="namef" placeholder="first name" class="form-control" />
		</div>
		<div class="formRow form-group">
		  <label class="fb-text-label">Last Name<span class="fb-required">*</span></label>
		  <input name="namel" type="text" maxlength="40"  id="namel" placeholder="last name" class="form-control"  />
		</div>

		<div class="formRow form-group"> 
		  <label class="fb-text-label">Postcode / Suburb<span class="fb-required">*</span></label>
		  <input name="postcode" type="text" maxlength="50"  id="postcode" placeholder="postcode / suburb" class="form-control"  />
		</div>
		<div class="formRow form-group">
		  <label class="fb-text-label">Mobile<span class="fb-required">*</span></label>
		  <input name="mobile" type="text" maxlength="20"  id="mobile" placeholder="mobile" class="form-control" />
		</div>
		<div class="formRow form-group">
		  <label class="fb-text-label">Email<span class="fb-required">*</span></label>
		  <input name="email" type="text" maxlength="255"  id="email" placeholder="email" class="form-control" />
		</div>
		<div class="formRowfat form-group">
		  <label class="fb-text-label">Your Requirements / Comments?<span class="fb-required">*</span></label>
		  <textarea name="info" id="info" rows="5" cols="20" placeholder="your requirements / comments?" class="form-control" ></textarea>
		</div>
		<div class="formRow form-group">
		  <input class="qbutton btn-submit" name="Submit" id="Submit"  type="submit" value="Submit" />
		</div>
	</div>
   
    <input name="region" id="region"  type="hidden" value="au" class="hidnfield"  />
    <input name="advicereq" id="advicereq"  type="hidden" value="Home Loan" class="hidnfield"  />
    <input name="companyId" type="hidden"   id="companyId" value="P200968"  />
    <input name="brokerId" type="hidden" id="brokerId" value="CA48476"  />
    <input name="notificationEmail" type="hidden"   id="notificationEmail" value="{{ $contact_email }}"  />
    <input name="notificationSms" type="hidden"   id="notificationSms" value="{{ $contact_sms }}"  />
    <input name="returnUrl" type="hidden"   id="returnUrl" value="{{ url('') }}/contact/success"  />
  </form>
</div>