<div class="col-sm-3 offset-sm-1 blog-sidebar">         
	<div class="sidebar-module">
	   <h4>Contact Us</h4>
	   
	   <ol class="navsidebar list-unstyled">             	   
	      <li class='{{ (isset($page_type) && $page_type == "contact" ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/contact">Contact Us</a></li>		
	      <li class='{{ ($module->slug == "teams" ? "active" : "")}}'><a class="navsidebar" href="{{ url('') }}/teams/">Our Team</a></li>
	      <li class='{{ (isset($page_type) && $page_type == "newsletter" ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/newsletter/">Newsletter</a></li>		
	   </ol>
	   
	   <br>
	   {!! $contact_details !!}
	</div>
</div>