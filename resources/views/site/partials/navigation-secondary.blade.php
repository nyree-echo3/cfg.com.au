<div class="navbar-secondary">
	@if(count($navigation))			   
	  @foreach($navigation as $nav_item)   
		@if ($nav_item["slug"] == "services")		           
			@if(isset($nav_item["nav_sub"])) 		               
			   @foreach($nav_item["nav_sub"] as $nav_sub)				                 
					 <a href="{{ url('') }}/pages/{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}">{{ $nav_sub["title"] }}</a>
			   @endforeach
			@endif
		@endif
	 @endforeach
  @endif
</div>