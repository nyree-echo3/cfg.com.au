<div class="home-services">
    @if(count($navigation))
        @foreach($navigation as $nav_item)
            @if ($nav_item["slug"] == "services")
                @if(isset($nav_item["nav_sub"]))
                    <div class='home-services-container'>
                        <div class="row">
                            @php
                                $colCounter = 0;
                            @endphp

                            @foreach($nav_item["nav_sub"] as $nav_sub)
                                @php
                                    $colCounter++;
                                @endphp

                                <div class="home-services-box home-services-col{{ $colCounter }}">
                                    <img src="{{ url('') }}/images/site/icon-services{{ $colCounter }}.png" alt="{{ $nav_sub["title"] }}"/>
                                    <h2>{{ $nav_sub["title"] }}</h2>
                                    <p>{{ $nav_sub["short_description"] }}</p>
                                    <a class="home-services-btn" href="{{ url('') }}/pages/{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}">Read more</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
    @endif
</div>