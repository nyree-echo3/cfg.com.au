@if(isset($home_links))
    <div class="panelLinks"> 
		<div class="home-links"> 

			<div class="links-carousel" 
			   data-cycle-pause-on-hover="true"
			   data-cycle-fx="carousel"
			   data-cycle-timeout="4000"
			   data-cycle-carousel-visible="9"
			   data-cycle-carousel-fluid="true"
			   data-cycle-slides="> span"
			>

				 @foreach($home_links as $item)       	 					          
				   @if($item->logo != "")				        
						 <span><a href="{{ $item->url }}" target="_blank"><img src="{{ url('') }}/{{ $item->logo }}" alt="{{ $item->title }}" /></a></span>					      
				   @endif					   					 					  				  
				 @endforeach 	

			   </div>
		</div>
    </div>
@endif
    
@section('scripts')    
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
     <script src="{{ asset('/components/jquery-cycle2/build/jquery.cycle2.min.js') }}"></script>
     <script src="{{ asset('js/site/jquery.cycle2.carousel.js') }}"></script>
@endsection

@section('inline-scripts')  
   <script type="text/javascript">        
            $.fn.cycle.defaults.autoSelector = '.links-carousel';        
    </script>			
@endsection