<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top bg-navbar btco-hover-menu">	
     <div class="navbar-logo">
     <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
  </div>
  
	<button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item {{ (!isset($page_type) ? "nav-link-active" : "") }}">
				<a class="nav-link" href="{{ url('') }}"><img src='{{ url('') }}/images/site/icon-home.png' alt="Home"><span class="sr-only">(current)</span></a>
			</li>
			@if(count($navigation))
               @foreach($navigation as $nav_item) 
                    @if ($nav_item["slug"] != "other")   
                    
                    @if ($nav_item["slug"] == "contact")
                        <li class="nav-item dropdown"><a class="nav-link {{ (isset($page_type) && $page_type == "calculators" ? "nav-link-active" : "") }}" href="{{ url('') }}/calculators" id="navbarDropdownMenuLink">Calculators</a></li>
                        <li class="nav-item dropdown"><a class="nav-link {{ (isset($page_type) && $page_type == "applynow" ? "nav-link-active" : "") }}" href="{{ url('') }}/apply-now" id="navbarDropdownMenuLink">Apply Now</a></li>
                    @endif
                    
					<li class="nav-item dropdown">
						<a class="nav-link {{ (isset($page_type) && (($page_type == "Pages" && $category[0]->slug == $nav_item["slug"]) || ($page_type != "Pages" && $page_type == $nav_item["slug"])) ? "nav-link-active" : "") }}" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}" id="navbarDropdownMenuLink">
							{{ $nav_item["display_name"] }}
						</a>
					
					@if ($nav_item["slug"] == "contact")
					    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				            <li><a class="dropdown-item" href="{{ url('') }}/contact">Contact Us</a></li>
					        <li><a class="dropdown-item" href="{{ url('') }}/teams">Our Team</a></li>
					        <li><a class="dropdown-item" href="{{ url('') }}/newsletter">Newsletter</a></li>
					    </ul>
					@endif
						
						@if(isset($nav_item["nav_sub"]) && sizeof($nav_item["nav_sub"]) > 0)
							<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							    
							    @foreach($nav_item["nav_sub"] as $nav_sub)	
								
									<li><a class="dropdown-item" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}">{{ (isset($nav_sub["title"]) ? $nav_sub["title"] : $nav_sub["name"]) }}</a>
									
									    @if(isset($nav_item["nav_sub_sub"]))
											<ul class="dropdown-menu">
												@foreach($nav_item["nav_sub_sub"] as $nav_sub_sub)	
												   @if($nav_sub_sub["parent_page_id"] == $nav_sub["id"])														  
													  <li><a class="dropdown-item" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}/{{ $nav_sub_sub["slug"] }}">{{ (isset($nav_sub_sub["title"]) ? $nav_sub_sub["title"] : $nav_sub_sub["name"]) }}</a></li>
												   @endif
												@endforeach											
											</ul>
										@endif
										
									</li>
								@endforeach
								
							</ul>
						@endif
						
					</li>
					@endif
			   @endforeach
            @endif
                                    			
		</ul>	  		  									
	</div>
</nav>
<!--
<div class="navbar-secondary">
	@if(count($navigation))			   
	  @foreach($navigation as $nav_item)   
		@if ($nav_item["slug"] == "services")		           
			@if(isset($nav_item["nav_sub"])) 		               
			   @foreach($nav_item["nav_sub"] as $nav_sub)				                 
					 <a href="{{ url('') }}/pages/{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}">{{ $nav_sub["title"] }}</a>
			   @endforeach
			@endif
		@endif
	 @endforeach
  @endif
</div>
-->
<div class='navbar-contacts'>
  <a href='tel:{{ str_replace(' ', '', $phone_number) }}'><span class='contact-icons'>T</span> {{ $phone_number }}</a>
  <a href='{{ url('') }}/contact'><span class='contact-icons'>E</span> {{ $contact_email }}</a>
</div>