<!--
https://support.connective.com.au/hc/en-us/articles/360008234993-Contact-us-and-Subscribe-to-newsletter
-->

<style>
	.the-carrot {
	 display: none;
	}
</style>
<!-- Validation Script -->
<script type="text/javascript">
	function validateForm(){
	var namef=document.forms["contact"]["namef"].value;
	var namel=document.forms["contact"]["namel"].value;
	var email=document.forms["contact"]["email"].value;
	if (namef==null || namef==""){  alert("Please enter your first name"); return false; }
	if (namel==null || namel==""){  alert("Please enter your last Name"); return false;}
	if (email==null || email==""){  alert("Please enter your email"); return false;}
}
</script>


<form id="contact" action="https://m5.connective.com.au/api/Leads/LeadHandler.jsp" method="POST" onsubmit="return validateForm()">
<!-- /////////// --> 
    <input  class="the-carrot" id="name" type="text" name="name" size="25" value="" autocomplete="off"/>
<!-- /////////// --> 
		<div class="rendered-form">	
			<!-- First Name-->
			<div class="formRow form-group">
				<label class="fb-text-label">First Name<span class="fb-required">*</span></label>
				<input name="namef" type="text" maxlength="40"  id="namef" placeholder="first name" class="form-control" />
			</div>
			<!-- Last Name-->
			<div class="formRow form-group">		
				<label class="fb-text-label">Last Name<span class="fb-required">*</span></label>	
				<input name="namel" type="text" maxlength="40" id="namel" placeholder="last name" class="form-control" />
			</div>
			<!-- Email-->
			<div class="formRow form-group">
				<label class="fb-text-label">Email<span class="fb-required">*</span></label>
				<input name="email" type="text" maxlength="255" id="email" placeholder="email" class="form-control" />
			</div>
			<div class="formRow form-group">
				<input class="qbutton btn-submit" name="Submit" id="Submit"  type="submit" value="Submit" />
			</div>
	    </div>
		<!-- Required input -->
	<input name="companyId" type="hidden" id="companyId" value="P200968" />
	<input name="brokerId" type="hidden" id="brokerId" value="CA48476" />
	<input name="returnUrl" type="hidden" id="returnUrl" value="{{ url('') }}/contact/success" />
	<input name="newsletterType" type="hidden" id="newsletterType" value="myMarketing">
	<input type="hidden" name="postcode" id="postcode" value="3105" />
		<!-- Not Required input -->
	<input name="notificationEmail" type="hidden" id="notificationEmail" value="{{ $contact_email }}" />
	<input name="notificationSms" type="hidden" id="notificationSms" value="{{ $contact_sms }}" />

</form>
