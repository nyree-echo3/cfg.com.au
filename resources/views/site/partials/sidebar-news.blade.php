<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>News</h4>
	<ol class="navsidebar list-unstyled">
	  @foreach ($side_nav as $item)
		 <li class=''><a class="navsidebar" href="{{ url('') }}/news/{{ $item->slug }}">{{ $item->name }}</a></li>               
	  @endforeach 	                                      
	</ol>
	
	<div class='btn-back'>
       <a class='btn-back' href='{{ url('') }}/news/archive'>ARCHIVED NEWS <i class='fa fa-chevron-right'></i></a>	
	</div>
  </div>          
</div>