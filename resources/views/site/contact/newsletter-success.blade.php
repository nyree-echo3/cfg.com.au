@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-contact')
                     
        <div class="col-sm-8 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Newsletter Sign Up</h1>    
               <p>Thank you for your sign up. You will receive our next newsletter shortly.</p>             
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
  </div><!-- /.blog-masthead -->  
@endsection            
