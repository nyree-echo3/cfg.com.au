@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')
@section('content')

	@include('site/partials/carousel-inner')

	<div class="blog-masthead">         
		<div class="container">
		  <div class="row">      
			@include('site/partials/sidebar-contact')        

			<div class="col-sm-8 blog-main">

			  <div class="blog-post">   
				   <h1 class="blog-post-title">Calculators</h1>  

				   @include('site/partials/calculators')

			  </div><!-- /.blog-post -->    
			</div><!-- /.blog-main -->         
		  </div><!-- /.row -->        

	  </div><!-- /.container -->
	</div><!-- /.blog-masthead -->

@endsection
@section('scripts')
	<script src="{{ asset('/components/bootstrap/js/dist/util.js') }}"></script>
@endsection