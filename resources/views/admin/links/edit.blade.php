@extends('admin/layouts/app')

@section('styles')   
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Links</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/links') }}"><i class="fas fa-link"></i> Links</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Links</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/links/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $links->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Title"
                                               value="{{ old('title',$links->title) }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SEO Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="slug" name="slug" class="form-control"
                                                   value="{{ old('slug',$links->slug) }}" readonly>
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      data-target="#change-slug">Change SEO Name
                                              </button>
                                            </span>
                                        </div>

                                        @if ($errors->has('slug'))
                                            <small class="help-block">{{ $errors->first('slug') }}</small>
                                        @endif
                                    </div>
                                </div>

                                @php
                                    if(old('category_id')!=''){
                                        $category_id = old('category_id');
                                    }else{
                                        $category_id = $links->category_id;
                                    }
                                @endphp
                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Category *</label>
                                    <div class="col-sm-10">
                                        <select name="category_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ ($category_id == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('url')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">URL *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="url" name="url"
                                               placeholder="URL"
                                               value="{{ old('title',$links->url) }}">
                                        @if ($errors->has('url'))
                                            <small class="help-block">{{ $errors->first('url') }}</small>
                                        @endif
                                    </div>
                                </div>
                               
                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="description"
                                                  placeholder="Description">{{ old('description',$links->description) }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                @php
                                    if(count($errors)>0){
                                       if(old('logo')!=''){
                                        $logo_image = old('logo');
                                       }else{
                                        $logo_image = '';
                                       }
                                    }else{
                                        $logo_image = $links->logo;
                                    }
                                @endphp
                                <div class="form-group {{ ($errors->has('logo')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Logo</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="logo" name="logo"
                                               value="{{ old('logo',$logo_image) }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($logo_image!=''){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($logo_image!='')
                                                <image src="{{ old('logo',$logo_image) }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('logo'))
                                            <small class="help-block">{{ $errors->first('logo') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $links->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status * </label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/links') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                                
                                <br><br>                             
                                <a href="{{ url('dreamcms/links/'.$links->id.'/preview') }}" class="pull-right" target="_blank"><i class="far fa-eye"></i>&nbsp;&nbsp;Page Preview</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$links->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            CKEDITOR.replace('description');

            $(".select2").select2();           

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#logo').val('');
            });

            $('#title').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });
        });

        function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#logo').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#logo').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection 