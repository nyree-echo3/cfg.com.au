<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Teams;
use App\TeamCategory;
use App\Module;
use App\Setting;
	
class TeamsController extends Controller
{
    public function index($category_slug = "", $mode = ""){
		$module = Module::where('slug', '=', "teams")->first();
		$side_nav = $this->getCategories();
		
		// Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();
		
		if (sizeof($side_nav) > 0)  {
			if ($category_slug == "")  {
			   $category_name = $side_nav[0]->name;	
				
			   $items = $this->getItems($side_nav[0]->id, $mode);		
			} else {			 
			  $category = $this->getCategory($category_slug);
			  $category_name = $category->name;	
				
			  $items = $this->getItems($category[0]->id, $mode);		
			}		
		}
		
		return view('site/teams/list', array(         
			'module' => $module,	
			'side_nav' => $side_nav,				
			'category_name' => (sizeof($side_nav) > 0 ? $category_name : null),
			'items' => (sizeof($side_nav) > 0 ? $items : null),	
			'mode' => $mode,
			'contact_details' => $contact_details->value,
        ));

    }
	
	public function getCategories(){
		$categories = TeamCategory::whereHas("teams")->where('status', '=', 'active')->get();		
		return($categories);
	}
	
	public function getCategory($category_slug){
		$categories = TeamCategory::where('slug', '=', $category_slug)->get();		
		return($categories);
	}
	
	public function getItems($category_id, $mode){
		if ($mode == "preview") {
		   $items = Teams::where('category_id', '=', $category_id)->orderBy('position', 'asc')->get();						
		} else {
		   $items = Teams::where('status', '=', 'active')->where('category_id', '=', $category_id)->orderBy('position', 'asc')->get();						
		}
		return($items);
	}		
}
