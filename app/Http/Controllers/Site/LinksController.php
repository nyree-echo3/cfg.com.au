<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Links;
use App\LinkCategory;

class LinksController extends Controller
{
    public function list(Request $request, $category_slug = "", $item_slug = ""){        		
    	$side_nav = $this->getCategories();	
		
		if ($category_slug == "")  {
		   // Get Latest News
		   $category_name = "Links";	
			
		   $items = $this->getLinks();
		} elseif ($category_slug != "" && $item_slug == "") {
		  // Get Category Links	
		  $category = $this->getCategory($category_slug);
		  $category_name = $category->name;	
			
		  $items = $this->getLinks($category->id);			  
		} 		
		
		return view('site/links/list', array(            			
			'side_nav' => $side_nav,
			'category_name' => $category_name,
			'items' => $items,			
        ));

    }
	
    public function item($category_slug, $item_slug, $mode = ""){
    	$side_nav = $this->getCategories();				  			
		$links_item = $this->getLinksItem($item_slug, $mode);			  
			
		return view('site/links/item', array(            			
			'side_nav' => $side_nav,					
			'links_item' => $links_item,	
			'mode' => $mode,
        ));
    }			
	
	public function getCategories(){
		$categories = LinkCategory::whereHas("links")->where('status', '=', 'active')->get();		
		return($categories);
	}	
	
	public function getLinks($category_id = "", $limit = 2){		
		if ($category_id == "")  {
			$links = Links::where('status', '=', 'active')												
						->paginate($limit);	
		} else {
		   $links = Links::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)												
						->paginate($limit);		
		}
		
		return($links);
	}
		
	public function getLinksItem($item_slug, $mode){		
		if ($mode == "preview") {
		   $links = Links::where(['slug' => $item_slug])->first();							
		} else {
		   $links = Links::where(['status' => 'active', 'slug' => $item_slug])->first();						
		}
		return($links);
	}
	
	public function getCategory($category_slug){
		$categories = LinkCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
}
