<?php

namespace App\Http\Controllers\Site;

use App\ContactMessages;
use App\Http\Controllers\Controller;
use App\Mail\ContactMessageAdmin;
use App\Mail\ContactMessageUser;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator, Illuminate\Support\Facades\Input, Redirect;

use App\Module;

class ContactController extends Controller
{
    public function index(){
        $module = Module::where('slug', '=', "contact")->first();
		
        $fields = Setting::where('key', '=', 'contact-form-fields')->first();

        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

		// Contact Email
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Contact SMS
		$setting = Setting::where('key','=','contact-sms')->first();
		$contactSMS = $setting->value;
		
        $form = $fields->value;

        if(old('name')){
            $fields = json_decode($fields->value);
            foreach ($fields as $field){
                $field->value = old($field->name);
            }

            $form = json_encode($fields);
        }

        return view('site/contact/contact', array(
			'module' => $module,
            'form' => $form,
            'contact_details' => $contact_details->value,
			'page_type' => "contact",
			'contact_email' => $contactEmail,
			'contact_sms' => $contactSMS
        ));
    }

    public function saveMessage(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('contact')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                     $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new ContactMessages();
        $message->data = json_encode($data);
        $message->save();

		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new ContactMessageAdmin($message));
		
		// Email User
        Mail::to($request->email)->send(new ContactMessageUser($message));

        return \Redirect::to('contact/success');
    }

    public function success(){
        $module = Module::where('slug', '=', "teams")->first();
		
		// Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

        return view('site/contact/success', array(
			'module' => $module,
			'contact_details' => $contact_details->value,
			'page_type' => "contact",
		));
    }
	
	public function newsletter(){
        $module = Module::where('slug', '=', "contact")->first();		        

        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

		// Contact Email
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Contact SMS
		$setting = Setting::where('key','=','contact-sms')->first();
		$contactSMS = $setting->value;		        

        return view('site/contact/newsletter', array(
			'module' => $module,            
            'contact_details' => $contact_details->value,
			'page_type' => "newsletter",
			'contact_email' => $contactEmail,
			'contact_sms' => $contactSMS
        ));
    }
	
	public function newsletterSuccess(){
        $module = Module::where('slug', '=', "teams")->first();
		
		// Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

        return view('site/contact/newsletter-success', array(
			'module' => $module,
			'contact_details' => $contact_details->value,
			'page_type' => "newsletter",
		));
    }
	
	public function applynow(){
        $module = Module::where('slug', '=', "contact")->first();		        

        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

		// Contact Email
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Contact SMS
		$setting = Setting::where('key','=','contact-sms')->first();
		$contactSMS = $setting->value;		        

        return view('site/contact/applynow', array(
			'module' => $module,            
            'contact_details' => $contact_details->value,
			'page_type' => "applynow",
			'contact_email' => $contactEmail,
			'contact_sms' => $contactSMS
        ));
    }
	
	public function applynowSuccess(){
        $module = Module::where('slug', '=', "teams")->first();
		
		// Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

        return view('site/contact/applynow-success', array(
			'module' => $module,
			'contact_details' => $contact_details->value,
			'page_type' => "applynow",
		));
    }
	
	public function calculators(){
        $module = Module::where('slug', '=', "contact")->first();		        

        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

		// Contact Email
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Contact SMS
		$setting = Setting::where('key','=','contact-sms')->first();
		$contactSMS = $setting->value;		        

        return view('site/contact/calculators', array(
			'module' => $module,            
            'contact_details' => $contact_details->value,
			'page_type' => "calculators",
			'contact_email' => $contactEmail,
			'contact_sms' => $contactSMS
        ));
    }
}
