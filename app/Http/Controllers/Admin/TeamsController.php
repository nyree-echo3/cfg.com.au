<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Teams;
use App\TeamCategory;
use App\Helpers\General;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TeamsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $teams = Teams::Filter()->sortable()->orderBy('created_at', 'desc')->paginate($paginate_count);
        } else {
            $teams = Teams::with('category')->sortable()->orderBy('created_at', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('teams-filter');
        $categories = TeamCategory::orderBy('created_at', 'desc')->get();
        return view('admin/teams/teams', array(
            'teams' => $teams,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = TeamCategory::orderBy('created_at', 'desc')->get();
        return view('admin/teams/add', array(
            'categories' => $categories
        ));
    }

    public function edit($teams_id)
    {
        $teams = Teams::where('id', '=', $teams_id)->first();
        $categories = TeamCategory::orderBy('created_at', 'desc')->get();
        return view('admin/teams/edit', array(
            'teams' => $teams,
            'categories' => $categories
        ));
    }

	public function preview($teams_id)
    {
		$teams = Teams::with("category")->where('id', '=', $teams_id)->first();		
		
		$general = new General();
		$view = $general->teamsPreview($teams->category->slug, $teams->slug);	
		
        return ($view);
    }
	
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'slug' => 'required|unique_store:teams', 
			'email' => 'required|string|email|max:255',
        );

        $messages = [
            'name.required' => 'Please enter name',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_store' => 'The SEO Name is already taken',
			'email.required' => 'Please enter email',
           
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/teams/add')->withErrors($validator)->withInput();
        }
       
        $teams = new Teams();
        $teams->category_id = $request->category_id;
        $teams->name = $request->name;
		$teams->slug = $request->slug;               
        $teams->short_description = $request->short_description;         
        $teams->description = $request->description;		
		$teams->role = $request->role;
		$teams->phone = $request->phone;
		$teams->mobile = $request->mobile;
		$teams->fax = $request->fax;
		$teams->email = $request->email;
        $teams->photo = $request->photo;

        if($request->live=='on'){
           $teams->status = 'active'; 
        }

        $teams->save();
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/teams/' . $teams->id . '/edit')->with('message', Array('text' => 'Team has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/teams/')->with('message', Array('text' => 'Team has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'slug' => 'required|unique_update:teams,' . $request->id,
            'email' => 'required|string|email|max:255',
        );

        $messages = [
            'name.required' => 'Please enter name',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_update' => 'The SEO Name is already taken',
            'email.required' => 'Please enter email',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/teams/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $teams = Teams::where('id','=',$request->id)->first();
        $teams->category_id = $request->category_id;
        $teams->name = $request->name;
        $teams->slug = $request->slug;       
        $teams->short_description = $request->short_description;       
        $teams->description = $request->description;
		$teams->role = $request->role;
		$teams->phone = $request->phone;
		$teams->mobile = $request->mobile;
		$teams->fax = $request->fax;
		$teams->email = $request->email;
        $teams->photo = $request->photo;
		if($request->live=='on'){
           $teams->status = 'active'; 
        } else {
			$teams->status = 'passive'; 
		}
        $teams->save();
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/teams/' . $teams->id . '/edit')->with('message', Array('text' => 'Team has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/teams/')->with('message', Array('text' => 'Team has been updated', 'status' => 'success'));
		}	
    }

    public function delete($teams_id)
    {
        $teams = Teams::where('id','=',$teams_id)->first();
        $teams->is_deleted = true;
        $teams->save();

        return \Redirect::back()->with('message', Array('text' => 'Team has been deleted.', 'status' => 'success'));
    }

    public function changeStatus(Request $request, $teams_id)
    {
        $teams = Teams::where('id', '=', $teams_id)->first();
        if ($request->status == "true") {
            $teams->status = 'active';
        } else if ($request->status == "false") {
            $teams->status = 'passive';
        }
        $teams->save();

        return Response::json(['status' => 'success']);
    }

    public function categories()
    {
        $categories = TeamCategory::orderBy('position', 'desc')->get();
        return view('admin/teams/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/teams/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:team_categories'
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/teams/add-category')->withErrors($validator)->withInput();
        }

        $category = new TeamCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();
 
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/teams/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/teams/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}	

    }

    public function editCategory($category_id)
    {
        $category = TeamCategory::where('id', '=', $category_id)->first();
        return view('admin/teams/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:team_categories,'.$request->id
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/teams/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = TeamCategory::findOrFail($request->id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
           $category->status = 'active'; 
		} else {
			$category->status = 'passive';
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/teams/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/teams/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		}
    }

    public function deleteCategory($category_id)
    {
        $category = TeamCategory::where('id','=',$category_id)->first();

        if(count($category->teams)){
            return \Redirect::to('dreamcms/teams/categories')->with('message', Array('text' => 'Category has teams. Please delete teams first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        return \Redirect::to('dreamcms/teams/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $team_category_id)
    {
        $team_category = TeamCategory::where('id', '=', $team_category_id)->first();
        if ($request->status == "true") {
            $team_category->status = 'active';
        } else if ($request->status == "false") {
            $team_category->status = 'passive';
        }
        $team_category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/teams/sort-category', array(
            'categories' => $categories
        ));
    }

    public function emptyFilter()
    {
        session()->forget('teams-filter');
        return redirect()->to('dreamcms/teams');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('teams-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('teams-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}