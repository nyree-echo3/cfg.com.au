<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Links;
use App\LinkCategory;
use App\Helpers\General;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class LinksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $links = Links::Filter()->sortable()->orderBy('created_at', 'desc')->paginate($paginate_count);
        } else {
            $links = Links::with('category')->sortable()->orderBy('created_at', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('links-filter');
        $categories = LinkCategory::orderBy('created_at', 'desc')->get();
        return view('admin/links/links', array(
            'links' => $links,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = LinkCategory::orderBy('created_at', 'desc')->get();
        return view('admin/links/add', array(
            'categories' => $categories
        ));
    }

    public function edit($links_id)
    {
        $links = Links::where('id', '=', $links_id)->first();
        $categories = LinkCategory::orderBy('created_at', 'desc')->get();
        return view('admin/links/edit', array(
            'links' => $links,
            'categories' => $categories
        ));
    }

	public function preview($links_id)
    {
		$links = Links::with("category")->where('id', '=', $links_id)->first();		
		
		$general = new General();
		$view = $general->linksPreview($links->category->slug, $links->slug);	
		
        return ($view);
    }
	
    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required',
			'slug' => 'required|unique_store:links',
            'url' => 'required',
			'logo' => 'required',
        );

        $messages = [
            'title.required' => 'Please enter title',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_store' => 'The SEO Name is already taken',
            'url.required' => 'Please enter url',
            'logo.required' => 'Please select a logo',            
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/links/add')->withErrors($validator)->withInput();
        }
       
        $links = new Links();
        $links->category_id = $request->category_id;
        $links->title = $request->title;
		$links->slug = $request->slug;        
        $links->url = $request->url;        
        $links->description = $request->description;
        $links->logo = $request->logo;
		
        if($request->live=='on'){
           $links->status = 'active'; 
        }

        $links->save();
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/links/' . $links->id . '/edit')->with('message', Array('text' => 'Links has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/links/')->with('message', Array('text' => 'Links has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(
            'title' => 'required',
			'slug' => 'required|unique_update:links,' . $request->id,
            'url' => 'required',                   
            'logo' => 'required',           
        );

        $messages = [
            'title.required' => 'Please enter title',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_update' => 'The SEO Name is already taken',
            'url.required' => 'Please enter url',                       
            'logo.required' => 'Please select a logo',           
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/links/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }
    
        $links = Links::where('id','=',$request->id)->first();
        $links->category_id = $request->category_id;
        $links->title = $request->title;
        $links->slug = $request->slug;
        $links->url = $request->url;       
        $links->description = $request->description;
        $links->logo = $request->logo;       
		if($links->live=='on'){
           $links->status = 'active'; 
        } else {
			$links->status = 'passive'; 
		}
        $links->save();
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/links/' . $links->id . '/edit')->with('message', Array('text' => 'Links has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/links/')->with('message', Array('text' => 'Links has been updated', 'status' => 'success'));
		}	
    }

    public function delete($links_id)
    {
        $links = Links::where('id','=',$links_id)->first();
        $links->is_deleted = true;
        $links->save();

        return \Redirect::back()->with('message', Array('text' => 'Links has been deleted.', 'status' => 'success'));
    }

    public function changeStatus(Request $request, $links_id)
    {
        $links = Links::where('id', '=', $links_id)->first();
        if ($request->status == "true") {
            $links->status = 'active';
        } else if ($request->status == "false") {
            $links->status = 'passive';
        }
        $links->save();

        return Response::json(['status' => 'success']);
    }

    public function categories()
    {
        $categories = linkCategory::orderBy('position', 'desc')->get();
        return view('admin/links/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/links/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:link_categories'
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/links/add-category')->withErrors($validator)->withInput();
        }

        $category = new LinkCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();
 
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/links/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/links/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}	

    }

    public function editCategory($category_id)
    {
        $category = LinkCategory::where('id', '=', $category_id)->first();
        return view('admin/links/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:link_categories,'.$request->id
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/links/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = LinkCategory::findOrFail($request->id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
           $category->status = 'active'; 
		} else {
			$category->status = 'passive';
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/links/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/links/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		}
    }

    public function deleteCategory($category_id)
    {
        $category = LinkCategory::where('id','=',$category_id)->first();

        if(count($category->links)){
            return \Redirect::to('dreamcms/links/categories')->with('message', Array('text' => 'Category has links. Please delete links first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        return \Redirect::to('dreamcms/links/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $link_category_id)
    {
        $link_category = LinkCategory::where('id', '=', $link_category_id)->first();
        if ($request->status == "true") {
            $link_category->status = 'active';
        } else if ($request->status == "false") {
            $link_category->status = 'passive';
        }
        $link_category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = LinkCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/links/sort-category', array(
            'categories' => $categories
        ));
    }

    public function emptyFilter()
    {
        session()->forget('links-filter');
        return redirect()->to('dreamcms/links');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('links-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('links-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}