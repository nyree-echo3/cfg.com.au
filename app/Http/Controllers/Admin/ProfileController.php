<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator, Illuminate\Support\Facades\Input, Redirect;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $user = User::find(Auth::User()->id);
        return view('admin/profile/profile', compact('user'));
    }

    public function saveInformation(Request $request)
    {

        $rules = array(
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique_update:users,'.Auth::User()->id
        );
        $messages = [
            'name.required' => 'Please enter a name',
            'email.required' => 'Please enter an email address',
            'email.email' => 'Please enter a valid email address',
            'email.unique_update' => 'The email address is in use'
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $user = User::find(Auth::User()->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return \Redirect::to('dreamcms/profile')->with('message', Array('text' => 'User information has been changed.', 'status' => 'success'));

    }

    public function changePassword(Request $request)
    {
        $rules = array(
            'current_password' => 'required',
            'password' => 'required|min:6|same:password',
            'password_confirmation' => 'required|same:password'
        );
        $messages = [
            'current_password.required' => 'Please enter your password',
            'password.required' => 'Please enter your new password',
            'password.min' => 'Your new password can not be less than 6 characters',
            'password_confirmation.required' => 'Please enter your new password',
            'password_confirmation.same' => 'Passwords does not match',
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator);
        }

        $current_password = Auth::User()->password;
        if(Hash::check($request->current_password, $current_password)){
            $user = User::find(Auth::User()->id);
            $user->password = Hash::make($request->password);
            $user->save();

            return \Redirect::to('dreamcms/profile')->with('message', Array('text' => 'Your password has been changed', 'status' => 'success'));
        }else{
            return \Redirect::to('dreamcms/profile')->with('message', Array('text' => 'Wrong password', 'status' => 'error'));
        }
    }
}
