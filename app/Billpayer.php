<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billpayer extends Model
{    
    protected $table = 'billpayers'; 
	
	public function addresses()
    {
        return $this->hasOne(Address::class,'id', 'address_id');
    }
}
