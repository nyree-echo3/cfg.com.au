<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Teams extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'teams';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function category()
    {
        return $this->belongsTo(TeamCategory::class, 'category_id');
    }
	public function categoryactive()
    {
        return $this->belongsTo(TeamCategory::class, 'category_id')->where('status', '=', 'active');
    }
    public function categorysort()
    {
        return $this->hasOne(TeamCategory::class,'id','category_id');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('teams-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
}
