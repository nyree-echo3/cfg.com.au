<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class TeamCategory extends Model
{
    use SortableTrait;

    protected $table = 'team_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function teams()
    {
        return $this->hasMany(Teams::class, 'category_id')->where('status', '=', 'active');
    }
	
	public function active_teams()
    {
        return $this->hasMany(Teams::class, 'category_id')->where('status', '=', 'active');
    }
}
