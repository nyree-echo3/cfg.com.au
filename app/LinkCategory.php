<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class LinkCategory extends Model
{
    use SortableTrait;

    protected $table = 'link_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function links()
    {
        return $this->hasMany(Links::class, 'category_id')->where('status', '=', 'active');
    }
	
	public function active_links()
    {
        return $this->hasMany(Links::class, 'category_id')->where('status', '=', 'active');
    }
}
