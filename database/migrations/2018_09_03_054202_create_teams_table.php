<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('category_id');
            $table->string('name');
            $table->string('slug');
            $table->text('role')->nullable();
            $table->text('short_description')->nullable();
			$table->text('description')->nullable();
			$table->text('phone')->nullable();
			$table->text('mobile')->nullable();
			$table->text('fax')->nullable();
			$table->text('email')->nullable();
            $table->text('photo')->nullable();            
            $table->enum('status', ['active','passive'])->default('passive');
			$table->enum('is_deleted', ['true','false'])->default('false');
            $table->integer('position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
